class CreateDrawings < ActiveRecord::Migration
  def change
    create_table :drawings do |t|
      
      t.string :drawing
      
      t.text :message
      t.string :sender
      t.string :url
      
      t.boolean :public

      t.timestamps
    end
    
    add_index :drawings, :url
  end
end
