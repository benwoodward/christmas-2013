require 'base64'

class CarrierStringIO < StringIO
  def original_filename
    "drawing.jpg"
  end
  def content_type
    "image/jpeg"
  end
end

class DrawingsController < ApplicationController

  def index
    @drawings = Drawing.where(public: true).order(created_at: :desc)
  end

  def show
    @drawing = Drawing.find_by url: params[:url]
    @request_url = request.original_url
  end

  def new
    @drawing = Drawing.new
  end

  def create
    @drawing = Drawing.new(drawing_params)
    
    data = params[:drawing][:drawing_data]
    
    image_data = Base64.decode64(data['data:image/jpeg;base64,'.length .. -1])
    @drawing.drawing = CarrierStringIO.new(image_data)
    
    if @drawing.save
      redirect_to drawing_path(url: @drawing.url), notice: 'yes'
    else
      render action: 'new'
    end
  end

  private
    def drawing_params
      params.require(:drawing).permit(:message, :sender, :public)
    end
end
