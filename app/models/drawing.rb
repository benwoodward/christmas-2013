class Drawing < ActiveRecord::Base

  before_create :generate_url
  
  mount_uploader :drawing, DrawingUploader
  attr_accessor :email
  
  private
  
    def generate_url
      self.url = loop do
        random_url = SecureRandom.urlsafe_base64[0..3]
        break random_url unless Drawing.exists?(url: random_url)
      end
    end
  
end
